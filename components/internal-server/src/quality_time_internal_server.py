"""Internal server."""

from fastapi import FastAPI


app = FastAPI()


@app.get("/")
async def health():
    """Return the health status."""
    return {"healthy": True}
