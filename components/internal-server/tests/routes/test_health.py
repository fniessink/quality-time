"""Unit tests for the health route."""

import unittest

from quality_time_internal_server import health


class HealthTestCase(unittest.IsolatedAsyncioTestCase):
    """Unit tests for the health route."""

    async def test_health(self):
        """Test the health route works."""
        self.assertEqual(dict(healthy=True), await health())
